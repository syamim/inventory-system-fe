import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { autoAnimatePlugin } from '@formkit/auto-animate/vue'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css'
import Multiselect from '@vueform/multiselect'
import '@vueform/multiselect/themes/default.css'

const app = createApp(App)
app.component('MultiSelect', Multiselect)
app.use(store)
app.use(router)
app.use(autoAnimatePlugin)
app.use(VueSweetalert2)
app.mount('#app')
window.Swal =  app.config.globalProperties.$swal;

