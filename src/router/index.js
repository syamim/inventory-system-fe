import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import ProductsView from '../views/ProductsView.vue'
import EditProductView from '../views/EditProductView.vue'
import SuppliersView from '../views/SuppliersView.vue'
import EditSupplierView from '../views/EditSupplierView.vue'
import StocksView from '../views/StocksView.vue'
import EditStockView from '../views/EditStockView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/stocks',
    name: 'stocks',
    component: StocksView
  },
  {
    path: '/stocks/:id',
    name: 'stock-edit',
    component: EditStockView
  },
  {
    path: '/suppliers',
    name: 'suppliers',
    component: SuppliersView
  },
  {
    path: '/suppliers/:id',
    name: 'supplier-edit',
    component: EditSupplierView
  },
  {
    path: '/products',
    name: 'products',
    component: ProductsView
  },
  {
    path: '/products/:id',
    name: 'product-edit',
    component: EditProductView
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
