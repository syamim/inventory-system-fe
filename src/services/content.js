import { authRequest } from './index.js'


export async function get_content(type, params = ''){
  let data
  await authRequest.get(
    `${type}?${params}`
  )
  .then(function(result){
    data = {result, error: false}
  })
  .catch(function (error){
    console.log(error)
    data = {error: true, result: error.response.data}
  })
  return data
}

export async function get_single_content(content_id, type, params = ''){
  let data
  await authRequest.get(
    `${type}/${content_id}?${params}`
  )
  .then(function(result){
    data = {result, error: false}
  })
  .catch(function (error){
    console.log(error)
    data = {error: true, result: error.response.data}
  })
  return data
}

export async function create_content(formData, type){
  let data
  await authRequest.post(
    `${type}/` ,
    {data: formData},
  )
  .then(function(result){
    data = {result, error: false}
  })
  .catch(function (error){
    console.log(error)
    data = {error: true, result: error.response.data}
  })
  return data
}

export async function delete_content(content_id, type){
  let data
  await authRequest.delete(
    `${type}/${content_id}`
  )
  .then(function(result){
    data = {result, error: false}
  })
  .catch(function (error){
    console.log(error)
    data = {error: true, result: error.response.data}
  })
  return data
}

export async function update_content(content_id, type, formData){
  let data
  await authRequest.put(
    `${type}/${content_id}`,
    { data: formData }
  )
  .then(function(result){
    data = {result, error: false}
  })
  .catch(function (error){
    console.log(error)
    data = {error: true, result: error.response.data}
  })
  return data
}